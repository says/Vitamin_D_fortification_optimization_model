# -*- coding: utf-8 -*-
"""
Created on Fri Sep 23 01:28:02 2022

@author: says
"""

import os
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import scipy
import re
from pulp import *

os.chdir('C:\\Users\\says\\Projects\\Vitamin D fortification\\data')
data_fgroup = pd.read_csv('fgroups.csv')
data_foodgroup = pd.read_excel('foodgroups.xlsx', sheet_name=0)

##First time data manipulation (comment them for normal use)

data_consumption1 = pd.read_csv('grams.csv')  
data_vitamin = pd.read_csv('vitD.csv')
data_consumption1['vitd'] = ''
data_consumption1['vitd'] = data_vitamin['IntakePrDay']


#group data according to age
#data_consumption = data_consumption1[data_consumption1.AgeGroup == '04-10']
#data_consumption = data_consumption1[data_consumption1.AgeGroup == '11-17']
#data_consumption = data_consumption1[data_consumption1.AgeGroup == '18-69']
data_consumption = data_consumption1
data_consumption.drop(data_consumption[data_consumption.MainFoodGroup == 99].index,inplace=True)


df1 = data_consumption.groupby(['MainFoodGroup'])['IntakePrDay','vitd'].apply(np.mean).reset_index()

#df1.drop(index=17,inplace=True)
#indices = df1[df1.vitd==0].MainFoodGroup
#data_consumption.drop(data_consumption[data_consumption.MainFoodGroup.isin(indices.values)].index,inplace=True)
#df1.drop(df1[df1.vitd==0].index,inplace=True)
#df1.reset_index(inplace=True)
df1['vitd_conc'] = df1['vitd']/df1['IntakePrDay']

# Linear optimisation starts here

prob = LpProblem("Simple Diet Problem",LpMaximize)
food_items = list(df1['MainFoodGroup'])
vitaminD = dict(zip(food_items,df1['vitd_conc']))
consumption = dict(zip(food_items,df1['IntakePrDay']))

vit_vars = LpVariable.dicts('Fortification',food_items,lowBound=0,cat='Continuous')

food_chosen = LpVariable.dicts('chosen',food_items,0,1,cat='Integer')

prob += lpSum([consumption[i]*vit_vars[i] for i in food_items])

prob += lpSum([consumption[i]*vit_vars[i] for i in food_items]) >= 7.5

prob += lpSum([consumption[i]*vit_vars[i] for i in food_items]) == 10.0

prob += lpSum([consumption[i]*vit_vars[i] for i in food_items]) <= 50.0

for f in food_items:
    prob += vit_vars[f]>= food_chosen[f]*0.1
    prob += vit_vars[f]<= food_chosen[f]*1e5

#prob += lpSum([food_chosen[i] for i in food_items]) >=9

for f in food_items:
    prob += vit_vars[f] <= vitaminD[f] + 0.2
    #prob += vit_vars[f] >= min(df1.vitd_conc)

prob.solve()

print("Status:", LpStatus[prob.status])

for v in prob.variables():
    print(v.name, "=", v.varValue)

i=0
a=[]
b=[]
for v in prob.variables():
    print(v.name, "=", v.varValue)
    if i<len(df1):
        
        a.append([int(s) for s in re.findall(r'\d+', v.name)][0])
        b.append(v.varValue)
        i=i+1        
vitamin_new =  dict(zip(a,b))       
        
obj = value(prob.objective)
print( obj)

#Revised vitamin D intake for the population with new estimate
data_consumption.drop(data_consumption[data_consumption.MainFoodGroup > 27].index,inplace=True)
data_consumption = data_consumption.reset_index()

data_consumption['optimal_vitamind'] = '' 

for i in range(len(df1)):
    data_consumption.loc[data_consumption['MainFoodGroup'] == df1.MainFoodGroup[i], 'optimal_vitamind'] = vitamin_new[df1.MainFoodGroup[i]]

df3=data_consumption.groupby(['Ipnr','MainFoodGroup'])['vitd'].apply(np.sum).reset_index()
df4=data_consumption.groupby(['Ipnr', 'AgeGroup'])['vitd'].apply(np.sum).reset_index()


#Calculate new Vitamin intake from revised estimates
df5=data_consumption.groupby(['Ipnr','MainFoodGroup','AgeGroup'])['IntakePrDay'].apply(np.sum).reset_index()
df5['vitamin_conc']=''
for i in range(len(df1)):
    df5.loc[df5['MainFoodGroup'] == df1.MainFoodGroup[i], 'vitamin_conc'] = vitamin_new[df1.MainFoodGroup[i]]
    
df5['Total_vitamin'] = df5['IntakePrDay'] * df5['vitamin_conc']
df6=df5.groupby(['Ipnr','AgeGroup'])['Total_vitamin'].apply(np.sum).reset_index()


#individual age group extraction from the final model of total population

# fig=plt.figure(num=None, figsize=(6,4),dpi=100, facecolor='w',edgecolor='k')
# plt.title('Population (70-75 years) ', size= 12)
# plt.xlabel('Vitamin D Intake ', size=10)
# plt.ylabel('Number of people', size= 10)
# plt.hist(df4.vitd[df4.AgeGroup == '70-75'], bins = 100)  
# plt.hist(df6.Total_vitamin[df6.AgeGroup == '70-75'], bins = 100)
# plt.axvline(x = 7.5, color = 'b', label = 'Average Requirement',lw=2)
# #plt.axvline(x = 10, color = 'k', label = 'Average Requirement',lw=2)
# plt.axvline(x = 100, color = 'r', label = 'Upper Limit',lw=2)   
# plt.savefig('Vitamin_intake_70-75_total_model.png')  
 
print('Total samples =' , len(df6[df6['AgeGroup']=='04-10']))
print('04-10 (above AR) old =' , len(df4[df4['AgeGroup']=='04-10'][df4.vitd >7.5]))
print('04-10 (above AR) new =' , len(df6[df6['AgeGroup']=='04-10'][df6.Total_vitamin >7.5]))

print('04-10 (above RI) old =' , len(df4[df4['AgeGroup']=='04-10'][df4.vitd >10]))
print('04-10 (above RI) new =' , len(df6[df6['AgeGroup']=='04-10'][df6.Total_vitamin >10]))

print('04-10 (below UL) old =' , len(df4[df4['AgeGroup']=='04-10'][df4.vitd <50]))
print('04-10 (below UL) new =' , len(df6[df6['AgeGroup']=='04-10'][df6.Total_vitamin <50]))

print('Total samples =' , len(df6[df6['AgeGroup']=='11-17']))
print('11-17 (above AR) old =' , len(df4[df4['AgeGroup']=='11-17'][df4.vitd >7.5]))
print('11-17 (above AR) new =' , len(df6[df6['AgeGroup']=='11-17'][df6.Total_vitamin >7.5]))
print('11-17 (above RI) old =' , len(df4[df4['AgeGroup']=='11-17'][df4.vitd >10]))
print('11-17(above RI) new =' , len(df6[df6['AgeGroup']=='11-17'][df6.Total_vitamin >10]))
print('11-17 (below UL) old =' , len(df4[df4['AgeGroup']=='11-17'][df4.vitd <100]))
print('11-17(below UL) new =' , len(df6[df6['AgeGroup']=='11-17'][df6.Total_vitamin <100]))


print('Total samples =' , len(df6[df6['AgeGroup']=='18-69']))
print('18-69 (above AR) old =' , len(df4[df4['AgeGroup']=='18-69'][df4.vitd >7.5]))
print('18-69 (above AR) new =' , len(df6[df6['AgeGroup']=='18-69'][df6.Total_vitamin >7.5]))
print('18-69 (above RI) old =' , len(df4[df4['AgeGroup']=='18-69'][df4.vitd >10]))
print('18-69(above RI) new =' , len(df6[df6['AgeGroup']=='18-69'][df6.Total_vitamin >10]))
print('18-69 (below UL) old =' , len(df4[df4['AgeGroup']=='18-69'][df4.vitd <100]))
print('18-69(below UL) new =' , len(df6[df6['AgeGroup']=='18-69'][df6.Total_vitamin <100]))


print('Total samples =' , len(df6[df6['AgeGroup']=='70-75']))
print('70-75 (above AR) old =' , len(df4[df4['AgeGroup']=='70-75'][df4.vitd >7.5]))
print('70-75 (above AR) new =' , len(df6[df6['AgeGroup']=='70-75'][df6.Total_vitamin >7.5]))
print('70-75 (above RI) old =' , len(df4[df4['AgeGroup']=='70-75'][df4.vitd >20]))
print('70-75(above RI) new =' , len(df6[df6['AgeGroup']=='70-75'][df6.Total_vitamin >20]))
print('70-75 (below UL) old =' , len(df4[df4['AgeGroup']=='70-75'][df4.vitd <100]))
print('70-75(below UL) new =' , len(df6[df6['AgeGroup']=='70-75'][df6.Total_vitamin <100]))



fig=plt.figure(num=None, figsize=(6,4),dpi=100, facecolor='w',edgecolor='k')
plt.title('Population (04-75 years) ', size= 12)
plt.xlabel('Vitamin D Intake ', size=10)
plt.ylabel('Number of people', size= 10)
plt.hist(df4.vitd, bins = 150)  
plt.hist(df6.Total_vitamin, bins = 150)
plt.axvline(x = 7.5, color = 'b', label = 'Average Requirement',lw=2)
#plt.axvline(x = 10, color = 'k', label = 'Average Requirement',lw=2)
plt.axvline(x = 50, color = 'r', label = 'Upper Limit',lw=2)   
# plt.savefig('Vitamin_intake_04-75.png')                  

print('Total samples =' , len(data_consumption.Ipnr.unique()))
print( "Old Mean = " ,df4.vitd.mean(),"Old Median =" , df4.vitd.median(),"Old AR =",len(df4[df4.vitd < 7.5 ]),"Old RI =" ,len(df4[df4.vitd < 10 ]),"Old UL =",len(df4[df4.vitd > 50 ])  )
print("New Mean =" ,df6.Total_vitamin.mean(),"New Median =" , df6.Total_vitamin.median(),"New AR =", len(df6[df6.Total_vitamin < 7.5 ]),"New RI =", len(df6[df6.Total_vitamin < 10 ]), "New UL =",len(df6[df6.Total_vitamin > 50 ]) )
    
